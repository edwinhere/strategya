// PFExperiment.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>

const enum Action {
	BUY = 1,
	DONOTHING = 0,
	SELL = -1
};


class PointFigure {
private:
	const unsigned int MAX_HISTORY;
	double box;
	const double reversal;
	const enum Direction {
		UP,
		DOWN
	};

	Direction direction;

	Action action;

	std::vector<double> prices;
	std::vector<int> columns;

	unsigned int i;
	unsigned int columnIndex; 
	unsigned int startIndex;
	double lastBox;

	inline int calculateColumns() {
		for(i = 0; i < prices.size(); i++) {
			columns[i] = 0;
		}

		lastBox = prices[0];

		for(i = 1; i < prices.size(); i++) {
			if(prices[i] - lastBox >= reversal * box) {
				do {
					if(lastBox + box > prices[i]) {
						break;
					}
					lastBox += box;
					columns[i]++;
					onColumnChange();
				} while(true);
				direction = UP;
				startIndex = i + 1;
				break;
			} else if(lastBox - prices[i] >= reversal * box) {
				do {
					if(lastBox - box < prices[i]) {
						break;
					}
					lastBox -= box;
					columns[i]--;
					onColumnChange();
				} while(true);
				direction = DOWN;
				startIndex = i + 1;
				break;
			} else {
			}
		}

		columnIndex = 1;

		updateColumns();

		return columnIndex;
	}

	void updateColumns() {
		for(i = startIndex; i < prices.size(); i++) {
			if(direction == UP) {
				if(prices[i] - lastBox >= box) {
					do {
						if(lastBox + box > prices[i]) {
							break;
						}
						lastBox += box;
						columns[columnIndex]++;
						onColumnChange();
					} while(true);
				} else if(lastBox - prices[i] >= reversal * box) {
					increaseColumns();
					columnIndex++;
					do {
						if(lastBox - box < prices[i]) {
							break;
						}
						lastBox -= box;
						columns[columnIndex]--;
						onColumnChange();
					} while(true);
					direction = DOWN;
				}
			} else if(direction == DOWN) {
				if(lastBox - prices[i] >= box) {
					do {
						if(lastBox - box < prices[i]) {
							break;
						}
						lastBox -= box;
						columns[columnIndex]--;
						onColumnChange();
					} while(true);
				} else if(prices[i] - lastBox >= reversal * box) {
					increaseColumns();
					columnIndex++;
					do {
						if(lastBox + box > prices[i]) {
							break;
						}
						lastBox += box;
						columns[columnIndex]++;
						onColumnChange();
					} while(true);
					direction = UP;
				}
			}
		}

		startIndex = i;
	}

	inline void increaseColumns() {
		if(columns.size() > 0) {
			onNewColumn();
		}
		columns.push_back(0);
	}

	inline void removePrices(int howMany) {
		if(prices.size() - howMany > 2) {
			prices.erase(prices.begin(), prices.begin() + howMany);
			startIndex -= howMany;
		}
	}

	inline void removeColumns(int howMany) {
		if(columns.size() - howMany > 2) {
			columns.erase(columns.begin(), columns.begin() + howMany);
			columnIndex -= howMany;
		}
	}

	inline void onNewColumn() {
		/*
		std::cout << columns[columns.size() - 1] << std::endl;
		*/

		if(columns.size() >= 3) {
			int a = columns[columns.size() - 3];
			int b = columns[columns.size() - 2];
			int c = columns[columns.size() - 1];

			if(a + (b - 1) + (c + 1) > a + 3 && c > 0) {
				//BUY
				action = BUY;
			} else if(a + (b + 1) + (c - 1) < a - 3 && c < 0) {
				//SELL
				action = SELL;
			} else {
				action = DONOTHING;
			}
		}
	}

	inline void onColumnChange() {
	}

public:
	PointFigure(double boxSize, double reversalAmount) : 
		MAX_HISTORY(60),
		box(boxSize) , 
		reversal (reversalAmount),
		prices(),
		columns() {
		i = 0;
		columnIndex = 0;
		startIndex = 0;
		lastBox = 0;
		direction = UP;
		action = DONOTHING;
	}

	void update(double tick) {
		prices.push_back(tick);
		if(prices.size() == 2) {
			increaseColumns();
			increaseColumns();
			calculateColumns();
		} else if(prices.size() > 2) {
			updateColumns();
		}

		if(prices.size() > MAX_HISTORY) {
			removePrices(MAX_HISTORY / 2);
		}

		if(columns.size() > MAX_HISTORY) {
			removeColumns(MAX_HISTORY / 2);
		}
	}

	Action getAction() {
		Action result = action;
		action = DONOTHING;
		return result;
	}

	void setBoxSize(double boxSize) {
		box = boxSize;
	}

	double getBoxSize() {
		return box;
	}

	void blankOut() {
		prices.clear();
		columns.clear();
		box = 1000.0;
		direction = DOWN;
		action = DONOTHING;
		i = 0;
		columnIndex = 0;
		startIndex = 0;
		lastBox = 0;
	}
};

#define MT4_EXPFUNC __declspec(dllexport)

std::vector<PointFigure> charts;

MT4_EXPFUNC int __stdcall createChart(const double box, const double reversal) {
	PointFigure pf(box, reversal);
	charts.push_back(pf);
	return charts.size() - 1;
}

MT4_EXPFUNC int __stdcall newTick(const int handle, const double tick) {
	charts[handle].update(tick);
	return (int) charts[handle].getAction();
}

MT4_EXPFUNC void __stdcall setBox(const int handle, const double boxSize) {
	charts[handle].setBoxSize(boxSize);
}

MT4_EXPFUNC double __stdcall getBox(const int handle) {
	return charts[handle].getBoxSize();
}

MT4_EXPFUNC void __stdcall cleanEverything() {
	charts.clear();
}

MT4_EXPFUNC void __stdcall cleanChart(const int handle) {
	charts[handle].blankOut();
}